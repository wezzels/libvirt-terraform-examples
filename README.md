# Terraform examples for libvirt plugin.

## Prerequisites.
# These are just history of commands. And most will need sudo.

#Installing golang, terraform and libvirt plugin
 wget https://releases.hashicorp.com/terraform/0.11.8/terraform_0.11.8_linux_amd64.zip
 apt -y install unzip
 unzip terraform_0.11.8_linux_amd64.zip
 mv terraform /usr/local/bin/
 apt install unzip git libvirt-dev
#Installing goLang
 add-apt-repository ppa:gophers/archive
 apt update
 apt install golang-1.10-go
# Fix and cat EOF sudo vi /etc/profile.d/golang110.sh
cat << EOF > /etc/profile.d/golang110.sh
  #!/bin/bash

  if [ -d "/usr/lib/go-1.10/bin" ] ; then
        export PATH="$PATH:/usr/lib/go-1.10/bin"
  fi
EOF
 source /etc/profile.d/golang110.sh
 export GOPATH=$HOME/go
 go version
 go get github.com/dmacvicar/terraform-provider-libvirt
 go install github.com/dmacvicar/terraform-provider-libvirt
 ls $GOPATH/bin/terraform-provider-libvirt
 terraform
 terraform init
 cd .terraform.d
 mkdir .terraform.d
 cd .terraform.d/
 mkdir plugins
 cd plugins/
 cp ~/go/bin/terraform-provider-libvirt .
