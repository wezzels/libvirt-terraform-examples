# We fetch the latest ubuntu release image from their mirrors
resource "libvirt_volume" "ubuntu-dockers-qcow2" {
  name = "${var.project}-ubuntu-dockers-qcow2"
  pool = "images"
  source = "imgs/xenial-server-cloudimg-amd64-disk1.img"
  format = "qcow2"
#  size = 5361393152
}

data "template_file" "meta_data_ubuntu_dockers" {
  template = "${file("${path.module}/cfgs/meta_data_ubuntu_dockers.cfg")}"
}


resource "libvirt_cloudinit_disk" "commoninit_ubuntu_dockers" {
          name           = "commoninit_ubuntu_dockers.iso"
          user_data          = "${data.template_file.user_data_dockers.rendered}"
          meta_data          = "${data.template_file.meta_data_ubuntu_dockers.rendered}"
          network_config = "${data.template_file.network_config.rendered}"
}


# Create the machine
resource "libvirt_domain" "domain-ubuntu-dockers" {
  name = "${var.project}-ubuntu-dockers-terraform"
  memory = "512"
  vcpu = 1

  cloudinit = "${libvirt_cloudinit_disk.commoninit_ubuntu_dockers.id}"

  network_interface {
    network_name = "vm_network"
  }

  # IMPORTANT
  # Ubuntu can hang if an isa-serial is not present at boot time.
  # If you find your CPU 100% and never is available this is why
  console {
    type        = "pty"
    target_port = "0"
    target_type = "serial"
  }

  console {
      type        = "pty"
      target_type = "virtio"
      target_port = "1"
  }

  disk {
       volume_id = "${libvirt_volume.ubuntu-dockers-qcow2.id}"
  }
  graphics {
    type = "spice"
    listen_type = "address"
    autoport = true
  }
}


# Print the Boxes IP
# Note: you can use `virsh domifaddr <vm_name> <interface>` to get the ip later
