# We fetch the latest ubuntu release image from their mirrors
resource "libvirt_volume" "ubuntu-plex-qcow2" {
  name = "${var.project}-ubuntu-plex-qcow2"
  pool = "images"
  source = "imgs/bionic-server-cloudimg-amd64.img"
  format = "qcow2"
#  size = 5361393152
}

data "template_file" "meta_data_ubuntu_plex" {
  template = "${file("${path.module}/cfgs/meta_data_ubuntu_plex.cfg")}"
}


resource "libvirt_cloudinit_disk" "commoninit_ubuntu_plex" {
          name           = "commoninit_ubuntu_plex.iso"
          user_data          = "${data.template_file.user_data_plex.rendered}"
          meta_data          = "${data.template_file.meta_data_ubuntu_plex.rendered}"
          network_config = "${data.template_file.network_config.rendered}"
}


# Create the machine
resource "libvirt_domain" "domain-ubuntu-plex" {
  name = "${var.project}-ubuntu-plex-terraform"
  memory = "512"
  vcpu = 1

  cloudinit = "${libvirt_cloudinit_disk.commoninit_ubuntu_plex.id}"

network_interface {
    bridge = "br0"
  }

  # IMPORTANT
  # Ubuntu can hang if an isa-serial is not present at boot time.
  # If you find your CPU 100% and never is available this is why
  console {
    type        = "pty"
    target_port = "0"
    target_type = "serial"
  }

  console {
      type        = "pty"
      target_type = "virtio"
      target_port = "1"
  }

  disk {
       volume_id = "${libvirt_volume.ubuntu-plex-qcow2.id}"
  }
  graphics {
    type = "spice"
    listen_type = "address"
    autoport = true
  }
}


# Print the Boxes IP
# Note: you can use `virsh domifaddr <vm_name> <interface>` to get the ip later
