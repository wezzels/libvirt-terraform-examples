
data "template_file" "user_data" {
  template = "${file("${path.module}/cfgs/cloud_init.cfg")}"
}

data "template_file" "user_data_af" {
  template = "${file("${path.module}/cfgs/cloud_init_af.cfg")}"
}

data "template_file" "user_data_gls" {
  template = "${file("${path.module}/cfgs/cloud_init_gls.cfg")}"
}

data "template_file" "user_data_glr" {
  template = "${file("${path.module}/cfgs/cloud_init_glr.cfg")}"
}

data "template_file" "user_data_gaf" {
  template = "${file("${path.module}/cfgs/cloud_init_gaf.cfg")}"
}

data "template_file" "network_config" {
  template = "${file("${path.module}/cfgs/network_config.cfg")}"
}

data "template_file" "user_data_desktop" {
  template = "${file("${path.module}/cfgs/cloud_init_desktop.cfg")}"
}

data "template_file" "user_data_plex" {
  template = "${file("${path.module}/cfgs/cloud_init_plex.cfg")}"
}



data "template_file" "user_data_dockers" {
  template = "${file("${path.module}/cfgs/cloud_init_dockers.cfg")}"
}

data "template_file" "user_data_openshift" {
  template = "${file("${path.module}/cfgs/cloud_init_openshift.cfg")}"
}


