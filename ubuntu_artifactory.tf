# We fetch the latest ubuntu release image from their mirrors
resource "libvirt_volume" "ubuntu-af-qcow2" {
  name = "${var.project}-ubuntu-af-qcow2"
  pool = "images"
  source = "imgs/xenial-server-cloudimg-amd64-disk1.img"
  format = "qcow2"
#  size = 5361393152
}

data "template_file" "meta_data_ubuntu_af" {
  template = "${file("${path.module}/cfgs/meta_data_ubuntu_af.cfg")}"
}


resource "libvirt_cloudinit_disk" "commoninit_ubuntu_af" {
          name           = "commoninit_ubuntu_af.iso"
          user_data          = "${data.template_file.user_data_af.rendered}"
          meta_data          = "${data.template_file.meta_data_ubuntu_af.rendered}"
          network_config = "${data.template_file.network_config.rendered}"
}


# Create the machine
resource "libvirt_domain" "domain-ubuntu-af" {
  name = "${var.project}-ubuntu-af-terraform"
  memory = "4096"
  vcpu = 1

  cloudinit = "${libvirt_cloudinit_disk.commoninit_ubuntu_af.id}"

  network_interface {
    network_name = "vm_network"
  }

  # IMPORTANT
  # Ubuntu can hang if an isa-serial is not present at boot time.
  # If you find your CPU 100% and never is available this is why
  console {
    type        = "pty"
    target_port = "0"
    target_type = "serial"
  }

  console {
      type        = "pty"
      target_type = "virtio"
      target_port = "1"
  }

  disk {
       volume_id = "${libvirt_volume.ubuntu-af-qcow2.id}"
  }
  graphics {
    type = "spice"
    listen_type = "address"
    autoport = true
  }
}


# Print the Boxes IP
# Note: you can use `virsh domifaddr <vm_name> <interface>` to get the ip later
