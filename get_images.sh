#!/bin/bash
#Create directory to put images in.
mkdir -p imgs

#Download images to improve performance. 
cd imgs
#Ubuntu and debian based.
curl -O https://cloud-images.ubuntu.com/releases/xenial/release/ubuntu-16.04-server-cloudimg-amd64-disk1.img
curl -O https://cloud-images.ubuntu.com/xenial/current/xenial-server-cloudimg-amd64-disk1.img

#CentOS 7.5
curl -O https://cloud.centos.org/centos/7/images/CentOS-7-x86_64-GenericCloud-1808.qcow2

#ClearOS does not work.  Will someday fix when custom requests.
curl -O https://cdn.download.clearlinux.org/image/clear-25180-cloud.img.xz
curl -O https://cdn.download.clearlinux.org/image/clear-25180-kvm.img.xz
unxz clear-25180-kvm.img.xz
unxz clear-25180-cloud.img.xz

$Opensuse
curl -O http://downloadcontent.opensuse.org/distribution/leap/15.0/jeos/openSUSE-Leap-15.0-JeOS.x86_64-15.0.1-OpenStack-Cloud-Snapshot20.56.qcow2

#Resize to 5G each
qemu-img resize openSUSE-Leap-15.0-JeOS.x86_64-15.0.1-OpenStack-Cloud-Snapshot20.56.qcow2 +5G
qemu-img resize xenial-server-cloudimg-amd64-disk1.img +5G
qemu-img resize CentOS-7-x86_64-GenericCloud-1808.qcow2 +5G
qemu-img resize ubuntu-16.04-server-cloudimg-amd64-disk1.img +5G

#Experimental
# Fedora
#curl -O https://download.fedoraproject.org/pub/fedora/linux/releases/28/Cloud/x86_64/images/Fedora-Cloud-Base-28-1.1.x86_64.qcow2

# FreeBSD
  #https://download.freebsd.org/ftp/releases/VM-IMAGES/11.2-RELEASE/amd64/Latest/FreeBSD-11.2-RELEASE-amd64.qcow2.xz
  #Must install cloud-init and follow the below instructions.  
  #https://docs.openstack.org/image-guide/freebsd-image.html
  # Older version but has cloud-init preinstalled. 
#http://images.openstack.nctu.edu.tw/bsd-cloudinit/freebsd-10.1-fls-cloudimage-20150501.qcow2
  #unxz FreeBSD-11.2-RELEASE-amd64.qcow2.xz

#Windows requires you to agree to licensing. 
  #There is a windows2012 eval you can try but will require you make your own."
  #https://cloudbase.it/windows-cloud-images/#download"
  # 6+Gig download.

#OSX MAC
  #Installing OSX has very limited use at this point.  Cloud-init is not supported. 
  # Found that it works but not too valuable without spice, cloud-init ... capabilities. 
  #https://github.com/kholia/OSX-KVM
